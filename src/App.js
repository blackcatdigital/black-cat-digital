import { HashRouter as Router } from 'react-router-dom';
import './App.css';
import Footer from './Components/Footer';
import Header from './Components/Header/index';
import Routes from './Routes';

function App() {
  return (
    <Router>
      <div className="app">
        <Header />
        <Routes />
        <Footer />
      </div>
    </Router>
  );
}

export default App;
