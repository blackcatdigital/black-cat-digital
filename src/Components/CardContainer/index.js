import React from 'react';
import './cardContainer.css';

const CardContainer = ({ children }) => (
    <div className='card-container'>
        {children}
    </div>
);

CardContainer.Card = ({ title, img }) => (
    <div className='card' style={{backgroundImage: `url(${img})`}} >
        <div className='title-container'>
            <h6>{title}</h6>
        </div>
        <div className='card-image' />
    </div>
)

export default CardContainer;